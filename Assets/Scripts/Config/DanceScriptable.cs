using System;
using UnityEngine;
using UnityEngine.Localization;

[CreateAssetMenu(fileName = "Dances", menuName = "LIfeIsTheGame/Dances")]
public class DanceScriptable : ScriptableObject {
    public Dance[] dances;
}

[Serializable]
public struct Dance {
    public string name;
    public LocalizedString localizedName;
}