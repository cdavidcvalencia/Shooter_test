using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Player", menuName = "LIfeIsTheGame/Player")]
public class PlayerScriptable : ScriptableObject {

    [Header("Look")]
    public float lookSensitivity = 10;

    [Header("Movement")]
    public float speed = 12f;
    public float gravity = -9.81f;
    public float jumpHeight = 3f;
    public float groundCheckDistance = 0.4f;
    public LayerMask groundMask;

}
