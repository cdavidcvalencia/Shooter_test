using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Weapons", menuName = "LIfeIsTheGame/Weapons")]
public class WeaponScriptable : ScriptableObject {

    [Header("Black Hole")]
    public float blackHoleCooldown;
    public float blackHoleBulletSpeed;
    public float blackHolePower;
    public float blackHoleDuration;
    public float blackHoleRange;
    public float blackHoleOrbitStrength;

    [Header("Anti Gravity")]
    public float antiGravityCooldown;
    public float antiGravityBulletSpeed;
    public float antiGravityPower;
    public float antiGravityDuration;

    [Header("Mortar")]
    public float MortarCooldown;
    public float MortarPower;

}
