using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackHoleBullet : MonoBehaviour {

    public LayerMask targetMask;

    PoolItem poolItem;
    Rigidbody body;
    float range;
    float duration;
    float power;
    float timer;
    float orbitStrength;
    public Collider[] targetResults = new Collider[1];

    private void Awake() {
        poolItem = GetComponent<PoolItem>();
        body = GetComponent<Rigidbody>();
        Deactivate();
    }

    public void Deactivate() {
        gameObject.SetActive(false);
        poolItem.isActive = false;
    }

    private void Update() {
        timer += Time.deltaTime;
        if (timer >= duration) {
            Deactivate();
        }
    }

    private void LateUpdate() {
        int numResults = Physics.OverlapSphereNonAlloc(transform.position, range, targetResults, targetMask);
        for(int idx = 0; idx < numResults; idx++) {
            Target target = targetResults[idx].GetComponent<Target>();
            Vector3 relativeDirection = (transform.position - target.transform.position).normalized;
            float ratio = Mathf.Clamp01(1 - (Vector3.Distance(transform.position, target.transform.position) / range));
            float currentPower = power * ratio;
            Vector3 orbitalDirection = Vector3.Cross(relativeDirection, transform.up);
            Vector3 force = relativeDirection + (orbitalDirection * orbitStrength);
            target.body.AddForce(currentPower * force);
        }
    }

    public void Fire(Transform muzzle, float speed, float inPower, float inDuration, float inRange, float inOrbitStrength) {
        orbitStrength = inOrbitStrength;
        range = inRange;
        power = inPower;
        timer = 0;
        duration = inDuration;
        gameObject.SetActive(true);
        body.isKinematic = true;
        body.velocity = Vector3.zero;
        transform.parent = muzzle;
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
        transform.parent = null;
        body.isKinematic = false;
        body.AddForce(transform.forward * speed);
    }

}
