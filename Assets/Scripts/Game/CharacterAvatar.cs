using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAvatar : MonoBehaviour {

    [Header("References")]
    public static CharacterAvatar instance;
    public Animator animator;

    public void Awake() {
        instance = this;
        this.transform.parent = null;
        DontDestroyOnLoad(this.gameObject);
    }
}
