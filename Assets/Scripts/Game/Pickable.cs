using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickable : MonoBehaviour {

    public Transform Model;
    public PickableType pickableType;

    public bool picked;

    public void SetPicked(bool inPicked) {
        picked = inPicked;
        gameObject.SetActive(!picked);
    }

}

public enum PickableType {
    mortar,
    blackHole,
    antiGravity
}