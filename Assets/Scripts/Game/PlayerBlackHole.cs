using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerBlackHole : MonoBehaviour
{
    public Transform muzzle;
    public float cooldown;
    public float bulletSpeed;
    public float power;
    public float duration;
    public float range;
    public float orbitStrength;

    bool readyToFire = true;
    float cooldownTimer = 0;

    Pool pool;
    PlayerInput playerInput;

    void Awake() {
        playerInput = new PlayerInput();
        playerInput.Player.Fire.performed += Fire;
    }

    private void OnEnable() {
        playerInput.Enable();
    }

    private void OnDisable() {
        playerInput.Disable();
    }

    private void Start() {
        pool = PoolManager.instance.GetPool("BlackHoleBullet");
    }

    private void Update() {
        if (readyToFire) return;
        cooldownTimer += Time.deltaTime;
        if (cooldownTimer >= cooldown) {
            readyToFire = true;
            cooldownTimer = 0;
        }
    }

    void Fire(InputAction.CallbackContext context) {
        if (!readyToFire) return;
        BlackHoleBullet bullet = pool.Get().GetComponent<BlackHoleBullet>();
        bullet.Fire(muzzle, bulletSpeed, power, duration, range, orbitStrength);
        readyToFire = false;
    }
}
