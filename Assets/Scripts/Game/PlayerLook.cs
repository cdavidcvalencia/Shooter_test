using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerLook : MonoBehaviour
{

    [Header("References")]
    public Transform player;

    [Header("Config")]
    public PlayerScriptable config;

    float xRotation;
    PlayerInput playerInput;
    bool looking = false;
    Vector2 inputLook;

    void Awake() {
        playerInput = new PlayerInput();
        playerInput.Player.Look.performed += Look;
        playerInput.Player.Look.canceled += StopLooking;
    }

    private void OnEnable() {
        playerInput.Enable();
    }

    private void OnDisable() {
        playerInput.Disable();
    }

    void Update() {
        if (!looking) return;
        float deltaTime = Time.deltaTime;
        float mouseX = inputLook.x * config.lookSensitivity * deltaTime;
        float mouseY = inputLook.y * config.lookSensitivity * deltaTime;
        player.Rotate(Vector3.up, mouseX);
        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90, 90);
        transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
    }

    public void Look(InputAction.CallbackContext context) {
        inputLook = context.ReadValue<Vector2>();
        looking = true;
    }

    public void StopLooking(InputAction.CallbackContext context) {
        looking = false;
    }

}
