using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour
{

    [Header("References")]
    public CharacterController controller;
    public Transform groundCheck;

    [Header("Config")]
    public PlayerScriptable config;

    Vector3 velocity;
    bool isGrounded;
    PlayerInput playerInput;
    Vector2 inputMovement;
    bool moving = false;
    bool jumping = false;

    void Awake() {
        playerInput = new PlayerInput();
        playerInput.Player.Move.performed += Move;
        playerInput.Player.Move.canceled += StopMoving;
        playerInput.Player.Jump.performed += Jump;
    }

    private void OnEnable() {
        playerInput.Enable();
    }

    private void OnDisable() {
        playerInput.Disable();
    }

    void FixedUpdate() {

        isGrounded = Physics.CheckSphere(groundCheck.position, config.groundCheckDistance, config.groundMask);

        if (isGrounded && velocity.y < 0) velocity.y = -2f;

        float deltaTime = Time.deltaTime;
        if (moving) {
            float x = inputMovement.x;
            float z = inputMovement.y;
            Vector3 move = transform.right * x + transform.forward * z;
            controller.Move(move * config.speed * deltaTime);
        }

        if (jumping) {
            velocity.y = Mathf.Sqrt(config.jumpHeight * -2f * config.gravity);
            jumping = false;
        }

        velocity.y += config.gravity * deltaTime;
        controller.Move(velocity * deltaTime);
    }

    public void Move(InputAction.CallbackContext context) {
        inputMovement = context.ReadValue<Vector2>();
        moving = true;
    }

    public void StopMoving(InputAction.CallbackContext context) {
        inputMovement = Vector2.zero;
        moving = false;
    }

    void Jump(InputAction.CallbackContext context) {
        if (!isGrounded) return;
        jumping = true;
    }
}
