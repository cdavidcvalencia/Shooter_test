using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolManager : MonoBehaviour {

    public static PoolManager instance;

    public int initialPoolSize = 10;
    public PoolItem[] itemTemplates;

    Dictionary<string, Pool> pools=new Dictionary<string, Pool>();

    private void Awake() {
        instance = this;
        DontDestroyOnLoad(this.gameObject);
    }

    public Pool GetPool(string poolName) {
        if (pools.ContainsKey(poolName)) {
            return pools[poolName];
        }
        int numTemplates = ArrayUtil.Length(itemTemplates);
        for(int idx = 0; idx < numTemplates; idx++) {
            if (itemTemplates[idx].name == poolName) {
                Pool pool = new Pool(initialPoolSize, itemTemplates[idx]);
                pools.Add(poolName, pool);
                return pool;
            }
        }
        return null;
    }

}

public class Pool {

    public List<PoolItem> items;

    PoolItem template;

    public Pool(int initialPoolSize, PoolItem inTemplate) {
        template = inTemplate;
        items = new List<PoolItem>(initialPoolSize);
        for (int idx = 0; idx < initialPoolSize; idx++) {
            items.Add(GameObject.Instantiate(template));
        }
    }

    public PoolItem Get() {
        int numItems = items.Count;
        for(int idx = 0; idx < numItems; idx++) {
            if (!items[idx].isActive) {
                items[idx].isActive = true;
                return items[idx];
            }
        }
        PoolItem item = GameObject.Instantiate(template);
        items.Add(item);
        return item;
    }

}