using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour {

    public Material mortarHit;
    public Material antigravityHit;
    public Renderer modelRenderer;
    public float mortarHintDuration = 2f;
    
    float antigravityHintDuration;

    Material defaultMaterial;
    public Rigidbody body;
    bool isMortarHit;
    bool isAntigravityHit;
    float mortarHitTimer;
    float antigravityHitTimer;

    private void Awake() {
        defaultMaterial = modelRenderer.material;
    }

    public void Update() {
        if (isMortarHit) {
            mortarHitTimer += Time.deltaTime;
            if (mortarHitTimer > mortarHintDuration) {
                if(isAntigravityHit) modelRenderer.material = antigravityHit;
                else modelRenderer.material = defaultMaterial;
                isMortarHit = false;
            }
        }
        if (isAntigravityHit) {
            antigravityHitTimer += Time.deltaTime;
            if (antigravityHitTimer > antigravityHintDuration) {
                modelRenderer.material = defaultMaterial;
                isAntigravityHit = false;
                body.useGravity = true;
            }
        }
    }

    public void MortarHit(Vector3 force) {
        modelRenderer.material = mortarHit;
        isMortarHit = true;
        mortarHitTimer = 0;
        body.AddForce(force);
    }

    public void AntiGravityHit(float duration,float power) {
        modelRenderer.material = antigravityHit;
        isAntigravityHit = true;
        antigravityHitTimer = 0;
        antigravityHintDuration = duration;
        body.useGravity = false;
        body.velocity = new Vector3(body.velocity.x, power, body.velocity.z);
    }

}
