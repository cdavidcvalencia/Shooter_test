using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Localization.Settings;

public class ConfigWindow : MonoBehaviour {

    public TMP_Dropdown language;

    private void Start() {
        switch (LocalizationSettings.SelectedLocale.Identifier.Code) {
            case "en":
                language.value = 0;
                break;
            case "es":
                language.value = 1;
                break;
        }
        language.RefreshShownValue();
    }

    public void LocaleChanged() {
        LocalizationSettings.SelectedLocale = LocalizationSettings.AvailableLocales.Locales[language.value];
    }

}
