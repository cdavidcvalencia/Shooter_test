using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.Localization.Settings;
using TMPro;

public class LocalizeDropdown : MonoBehaviour {

    public LocalizedString[] dropdownOptions = new LocalizedString[0];
    public Sprite[] images = new Sprite[0];
    public TMP_Dropdown tmpDropdown;

    Locale currentLocale;

    private void Start() {
        RefreshOptions();
        LocalizationSettings.SelectedLocaleChanged += ChangedLocale;
    }

    void RefreshOptions() {
        List<TMP_Dropdown.OptionData> tmpDropdownOptions = new();
        int numOptions = dropdownOptions.Length;
        for (int i = 0; i < numOptions; i++) {
            TMP_Dropdown.OptionData optionData = new TMP_Dropdown.OptionData(
                dropdownOptions[i].GetLocalizedString(),
                images[i]
            );
            tmpDropdownOptions.Add(optionData);
        }
        tmpDropdown.options = tmpDropdownOptions;
    }

    private void ChangedLocale(Locale newLocale) {
        if (currentLocale == newLocale) return;
        currentLocale = newLocale;
        RefreshOptions();
    }

}